#!/bin/sh

docker run -d --name rethinkdb-test -p 30001:28015 rethinkdb:2.4.1-buster-slim rethinkdb --bind all --initial-password 123456

sleep 5

python -W ignore::DeprecationWarning -m unittest

docker stop rethinkdb-test && docker rm rethinkdb-test